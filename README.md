# var_dump

A web-extension to beautify PHP `var_dump`, `var_export` and `print_r` outputs.

![illustration](com/illustration.png)

## How to build

used programs :

| Program | Version |
|---------|---------|
| Bash    | 5.0.17  |
| Node.js | 13.10.1 |
| NPM     | 6.14.7  |
| Zip     | 3.0     |

### 1. install the dependencies

`npm install`

### 2. build from the sources

`npm run build`

### 3. create the distributable archive

`npm run distribute`

Then, the archive can be found in the `dist` folder.
