#!/bin/bash

BUILD_FOLDER="build"
DIST_FOLDER="dist"
ARCHIVE_NAME="${npm_package_name}-${npm_package_version}.zip"

# Create 'dist' folder if it doesn't already exist.
if [ ! -d "$DIST_FOLDER" ]; then
	mkdir "$DIST_FOLDER"
fi

# Remove archive if it already exists.
if [ -f "${DIST_FOLDER}/${ARCHIVE_NAME}" ]; then
	rm -f "${DIST_FOLDER}/${ARCHIVE_NAME}"
fi

cd "$BUILD_FOLDER"

# make archive
zip -r "../${DIST_FOLDER}/${ARCHIVE_NAME}" *
