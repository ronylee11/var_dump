import { decode } from 'he'
import { registerLanguage, highlightAuto } from 'highlight.js/lib/core'
import hljsPhp from 'highlight.js/lib/languages/php'
// @ts-ignore
import style from './style.scss'

registerLanguage('php', hljsPhp)

const customHead = `<meta charset="utf-8"><style>${style}</style>`

let originalHead: string = undefined
let originalBody: string = undefined
let currentlyDisplayingOriginal = true

browser.runtime.onMessage.addListener((request: {action?: string}) => {
	if (request.action === 'iconClicked') onIconClicked()
})

function onIconClicked() {
	if (currentlyDisplayingOriginal) {
		// save the original document so the operation can be reverted
		originalHead = document.head.innerHTML
		originalBody = document.body.innerHTML
		// load the custom head
		document.head.innerHTML = customHead
		// beautify the body (containing PHP var_dump output)
		document.body.innerHTML = beautify(originalBody)
	} else {
		// revert back to the original document
		document.head.innerHTML = originalHead
		document.body.innerHTML = originalBody
		// save space
		originalHead = undefined
		originalBody = undefined
	}
	currentlyDisplayingOriginal = !currentlyDisplayingOriginal
}

function beautify(rawHtml: string): string {
	const formatted = highlightAuto(decode(rawHtml), ['php']).value
	return `<pre><code class="hljs">${formatted}</code></pre>`
}
