import 'regenerator-runtime/runtime'

// When the icon is clicked...
browser.browserAction.onClicked.addListener(async () => {
	// ...get the active tab...
	const tab = (await browser.tabs.query({active: true, currentWindow: true}))[0]
	// ...and notify it of the click.
	await browser.tabs.sendMessage(tab.id, {action: 'iconClicked'})
})
